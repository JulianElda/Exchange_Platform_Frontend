'use strict';

(function () {
  var app = angular.module('ep-frontend', ['ui.router', 'ui.bootstrap', 'ngCookies', 'ngFileSaver']);
  app.config(function ($urlRouterProvider, $stateProvider, $locationProvider) {

    // define the states and their parameters
    // https://github.com/angular-ui/ui-router/wiki/URL-Routing
    $stateProvider.state({
      name: 'catalog',
      url: '/',
      template: '<catalog></catalog>'
    }).state({
      name: 'lab',
      url: '/lab/:id',
      params: {
        id: null
      },
      template: '<lab-info></lab-info>'
    }).state({
      name: 'solutions',
      params: {
        id: null,
        solution: null
      },
      template: '<lab-solutions></lab-solutions>'
    }).state({
      name: 'survey_confirm',
      url: '/survey?id&token&branch&sha',
      template: '<survey-confirm></survey-confirm>'
    }).state({
      name: 'survey_export',
      params: {
        id: null,
        feedback: null
      },
      template: '<survey-export></survey-export>'
    }).state({
      name: 'survey_form',
      params: {
        id: null,
        token: null,
        branch: null,
        sha: null
      },
      template: '<course-survey></course-survey>'
    }).state({
      name: 'survey_result',
      params: {
        id: null
      },
      template: '<survey-result></survey-result>'
    }).state({
      name: 'survey_token',
      params: {
        id: null
      },
      template: '<survey-token></survey-token>'
    }).state({
      name: 'help',
      url: '/help',
      template: '<help-page></help-page>'
    });

    $urlRouterProvider.otherwise('/');

    // TODO enable html5 mode to remove hashbang in the url
    // https://stackoverflow.com/questions/22102815/how-to-delete-sign-in-angular-ui-router-urls
    //$locationProvider.html5Mode(true);
  }).run(function ($rootScope, $http) {
    // get frontend configuration
    /*
    $http.get('/config.json').then(function(data){
      $rootScope.config = data.data;
    })
    */

    //temporary fix
    $rootScope.config = {
      "hostname": "http://ilabxp2.net.in.tum.de",
      "git_host": "http://ilabxp2.net.in.tum.de:9080",
      "git_api": "api/v4",
      "db_host": "http://ilabxp2.net.in.tum.de",
      "portal_token": "3kQUqRuDGfmrr51JqYj8",
      "portal_uid": "1331504",
      "portal_comment_alias": "Anonymous",
      "lab_preview_path_prefix": "preview",
      "issue_comments_name": "_comments",
      "labs_group_id": 3,
      "solutions_group_id": 4,
      "solutions_group_name": "_solutions",
      "feedback_group_id": 6,
      "feedback_group_name": "_feedback",
      "survey_backup_name": "survey.json"
    };
  });
})();
'use strict';

(function () {
  'use strict';

  angular.module('ep-frontend').component('catalog', {
    templateUrl: '/app/components/catalog/catalog.html',
    controller: CatalogCtrl,
    controllerAs: 'cc'
  });
  function CatalogCtrl($scope, $state, util, config, glw) {
    var vm = this;

    vm.selectedTag = '';
    vm.labs = [];

    vm.tagFilter = function (item) {
      if (vm.selectedTag != '') return item.tag_list.includes(vm.selectedTag);else return item;
    };

    // get labs, i.e. projects
    // since the labs in the catalog are in _labs group
    // we get the list of projects in that _labs group
    function getLabs() {
      var success = function success(res) {
        vm.labs = vm.labs.concat(res
        //map all the tag_list of a project for filtering
        );var tag_list = vm.labs.map(function (project) {
          return project.tag_list;
        });
        vm.tags = [].concat.apply([], tag_list);
        vm.tags = util.onlyUnique(vm.tags);
      };
      glw.group_projects(config.labs_group_id, success);
    }

    function init() {
      getLabs();
    }

    init();
  }
})();
'use strict';

(function () {
  'use strict';

  angular.module('ep-frontend').component('newsFeed', {
    templateUrl: '/app/components/feed/feed.html',
    controller: FeedCtrl,
    controllerAs: 'nf'
  });
  function FeedCtrl($scope, glw) {
    var vm = this;

    function eventText(event) {
      if (event.action_name == 'commented on') {
        return 'New comment on ' + event.target_title;
      } else if (event.action_name == 'pushed to') {
        return event.data.project.name + ' was updated';
      } else if (event.action_name == 'pushed new') {
        return event.data.project.name + ' was created';
      }
    }

    // we only show these events
    // for future works this can be futher implemented & improved
    function getEvents() {
      var success = function success(res) {
        vm.events = res.filter(function (item) {
          return item.action_name == 'commented on' || item.action_name == 'pushed to' || item.action_name == 'pushed new';
        }).map(eventText);
      };
      glw.events(success);
    }

    function init() {
      getEvents();
    }

    init();
  }
})();
'use strict';

(function () {
  'use strict';

  angular.module('ep-frontend').component('helpContent', {
    templateUrl: '/app/components/help/help.html',
    controller: HelpContentCtrl,
    controllerAs: 'hcc'
  });
  function HelpContentCtrl($scope, $state, util, config) {
    var vm = this;
  }
})();
'use strict';

(function () {
  'use strict';

  angular.module('ep-frontend').component('helpPage', {
    templateUrl: '/app/components/help/help_page.html',
    controller: HelpCtrl,
    controllerAs: 'hc'
  });
  function HelpCtrl($scope, $state, util, config) {
    var vm = this;
  }
})();
'use strict';

(function () {
  'use strict';

  angular.module('ep-frontend').component('labComments', {
    bindings: {
      ids: '<'
    },
    templateUrl: '/app/components/lab/comments.html',
    controller: LabCommentsCtrl,
    controllerAs: 'lc'
  });
  function LabCommentsCtrl($scope, auth, config, glw) {
    var vm = this;

    vm.is_auth = auth.isAuthenticated();

    vm.postComments = function () {
      var success = function success(res) {
        vm.comments.length = 0;
        getComments();
      };
      glw.new_note(vm.feedback_project_id, vm.issue_id, vm.new_comment, success);
    };

    vm.humanizeComment = function (dt) {
      return moment(dt).fromNow();
    };

    function getComments() {
      var success = function success(res) {
        vm.comments = res;
      };
      glw.issue_notes(vm.feedback_project_id, vm.issue_id, success);
    }

    // unlike every other module, this has to run on vm..$onInit()
    // since it loads after the /lab page finishes loading
    vm.$onInit = function () {
      vm.feedback_project_id = vm.ids.feedback_project_id;
      vm.issue_id = vm.ids.issue_id;
      getComments();
    };
  }
})();
'use strict';

(function () {
  'use strict';

  angular.module('ep-frontend').component('labInfo', {
    templateUrl: '/app/components/lab/lab.html',
    controller: LabInfoCtrl,
    controllerAs: 'li'
  });
  function LabInfoCtrl($scope, $state, $stateParams, config, glw) {
    var vm = this;

    vm.project_id = $stateParams.id;

    vm.prefix = config.lab_preview_path_prefix + '/';

    vm.permissions = {};
    vm.loading = {};

    vm.download = function (item) {
      var success = function success(res) {};
      glw.blob(item.id, 'master', success);
    };

    // not used at the moment
    // returns the download link of the project in archive format
    vm.downloadRepo = function (branch) {
      return glw.archive_url(vm.project_id, branch.name);
    };

    function getBranches() {
      var success = function success(res) {
        vm.branches = res;
      };
      glw.branches(vm.project_id, success);
    }

    function getIssues() {
      var success = function success(res) {
        //there might be other issues created in GitLab, so we filter by name
        vm.issue_id = res.filter(function (item) {
          return item.title == vm.lab.name + config.issue_comments_name;
        })[0].iid;

        vm.ids = {
          project_id: vm.project_id,
          issue_id: vm.issue_id,
          feedback_project_id: vm.feedback_project_id
        };

        vm.loading.issue = true;
      };
      glw.project_issues(vm.feedback_project_id, success);
    }

    // these are the unfortunate drawback of the schema, since
    // when I get the lab info, I have to find the solutions/feedback
    // id by filtering them by name like this

    function getFeedback() {
      var success = function success(res) {
        vm.feedback_project_id = res.filter(function (item) {
          return item.name == vm.lab.name + config.feedback_group_name;
        })[0].id;
        getIssues();
      };
      glw.group_projects(config.feedback_group_id, success);
    }

    function getSolutions() {
      var success = function success(res) {
        vm.permissions.solutions = true;
        vm.solutions_project_id = res.filter(function (item) {
          return item.name == vm.lab.name + config.solutions_group_name;
        })[0].id;
      };
      glw.group_projects(config.solutions_group_id, success);
    }

    // get the lab information and its solutions and feedback project
    function getProject() {
      var success = function success(res) {
        vm.lab = res;

        // if you have at least Master permission
        // see https://docs.gitlab.com/ee/api/members.html
        if (vm.lab.permissions.group_access != null && vm.lab.permissions.group_access.access_level >= 40 || vm.lab.permissions.project_access != null && vm.lab.permissions.project_access.access_level >= 40) vm.permissions.survey = true;

        // we only want to get the id of these projects
        getSolutions();
        getFeedback();
      };
      glw.project(vm.project_id, success);
    }

    function init() {
      getProject();
    }

    init();
  }
})();
'use strict';

(function () {
  'use strict';

  angular.module('ep-frontend').component('labSolutions', {
    templateUrl: '/app/components/lab/solutions.html',
    controller: LabSolutionsCtrl,
    controllerAs: 'ls'
  });
  function LabSolutionsCtrl($scope, $state, $stateParams, config, glw) {
    var vm = this;

    vm.project_id = $stateParams.id;
    vm.solutions_project_id = $stateParams.solution;

    vm.permissions = {};
    vm.loading = {};

    function getProject() {
      var success = function success(res) {
        vm.solution = res;
      };
      glw.project(vm.solutions_project_id, success);
    }

    function init() {
      getProject();
    }

    init();
  }
})();
'use strict';

(function () {
  'use strict';

  angular.module('ep-frontend').component('mainHeader', {
    templateUrl: '/app/components/nav/header.html',
    controller: HeaderCtrl,
    controllerAs: 'hc'
  });
  function HeaderCtrl($rootScope, $state, glw, auth) {
    var vm = this;

    vm.login = function () {
      var success = function success(res) {
        vm.form = false;
        vm.username = '';
        vm.password = '';

        //set private token to cookie
        auth.setUser(res);
        vm.user = res;

        $state.go('catalog');
      };
      glw.login(vm.username, vm.password, success);
    };

    vm.logout = function () {
      auth.clear();
      vm.user = undefined;

      $state.go('catalog');
    };

    vm.user = auth.getUser();
  }
})();
'use strict';

(function () {
  'use strict';

  angular.module('ep-frontend').component('surveyConfirm', {
    templateUrl: '/app/components/survey/survey_confirm.html',
    controller: SurveyConfirmCtrl,
    controllerAs: 'sc'
  });
  function SurveyConfirmCtrl($scope, $state, $stateParams, config, dbw, glw) {
    var vm = this;

    vm.project_id = $stateParams.id;
    vm.branch = $stateParams.branch;
    vm.sha = $stateParams.sha;
    vm.token = $stateParams.token;

    vm.confirm = function () {
      $state.go('survey_form', {
        id: vm.project_id,
        token: vm.token_id,
        branch: vm.branch,
        sha: vm.sha
      });
    };

    function getProject() {
      var success = function success(res) {
        vm.info = res;
      };
      glw.project(vm.project_id, success);
    }

    // send a query to the db asking for this token
    // if it returns something then the token is valid
    function verifyToken() {
      var success = function success(res) {
        var valid = res.survey_token;
        if (valid.length < 1) vm.invalid = true;

        var token_date = new Date(valid[0].expiration_date);
        var now = new Date();

        if (now.getTime() > token_date.getTime()) vm.invalid = true;

        vm.token_id = valid[0].id;
      };
      dbw.token_verify(vm.project_id, vm.token, success);
    }

    function init() {
      getProject();
      verifyToken();
    }

    init();
  }
})();
'use strict';

(function () {
  'use strict';

  angular.module('ep-frontend').component('surveyExport', {
    templateUrl: '/app/components/survey/survey_export.html',
    controller: SurveyExportCtrl,
    controllerAs: 'se'
  });
  function SurveyExportCtrl($scope, $state, $stateParams, FileSaver, config, dbw, glw) {
    var vm = this;

    vm.project_id = $stateParams.id;
    vm.feedback_project_id = $stateParams.feedback;
    vm.branch = '';
    vm.commit = '';

    // make a commit to _feedback project to save the survey data
    // now it only commits to master branch
    // also, this will fail if the file doesnt exist
    // this file is commited in the initial script, but you might want to make sure it exist
    vm.export_git = function () {
      var success = function success(res) {
        vm.git_success = true;
      };

      glw.commit(vm.feedback_project_id, 'master', vm.commit_msg, [{
        action: 'update',
        file_path: config.survey_backup_name,
        content: JSON.stringify(vm.results)
      }], success);
    };

    // save to browser
    vm.export_json = function () {
      var text = JSON.stringify(vm.results);
      var data = new Blob([text], { type: 'application/json;charset=utf-8' });
      FileSaver.saveAs(data, vm.filename + '.json');
    };

    // remember that the frontend filters by SHORT ID not the long id
    vm.getCommits = function () {
      var success = function success(res) {
        vm.commits = res;
      };
      glw.commits(vm.project_id, vm.branch, success);
    };

    function getBranches() {
      var success = function success(res) {
        vm.branches = res;
      };
      glw.branches(vm.project_id, success);
    }

    vm.getLabFeedbacks = function () {
      var success = function success(res) {
        vm.results = res.survey_result;
      };
      dbw.survey_list(vm.project_id, vm.branch, vm.commit, success);
    };

    // not used, in case you wanna export the token too
    function getFeedbackTokens() {
      var success = function success(res) {
        vm.tokens = res.survey_token;
      };
      dbw.token_list(vm.project_id, success);
    }

    function init() {
      getBranches();
    }

    init();
  }
})();
'use strict';

(function () {
  'use strict';

  angular.module('ep-frontend').component('courseSurvey', {
    templateUrl: '/app/components/survey/survey_form.html',
    controller: CourseSurveyCtrl,
    controllerAs: 'cs'
  });
  function CourseSurveyCtrl($scope, $state, $stateParams, config, dbw, glw) {
    var vm = this;

    vm.project_id = $stateParams.id;
    vm.token = $stateParams.token;

    // save the survey answers to database, then delete the token
    vm.submitSurvey = function () {
      var success = function success(res) {
        updateToken();
      };
      vm.survey.branch = $stateParams.branch;
      vm.survey.sha = $stateParams.sha;
      vm.survey.project_id = $stateParams.id;
      vm.survey.s1 = parseInt(vm.survey.s1);
      vm.survey.s2 = parseInt(vm.survey.s2);
      vm.survey.s3 = parseInt(vm.survey.s3);

      dbw.survey_new(vm.survey, success);
    };

    // delete used token, then go back to the lab page
    function updateToken() {
      var success = function success(res) {
        $state.go('lab', { id: vm.project_id });
      };

      dbw.token_delete(vm.token, success);
    }

    function getProject() {
      var success = function success(res) {
        vm.info = res;
      };
      glw.project(vm.project_id, success);
    }

    function init() {
      getProject();
    }

    init();
  }
})();
'use strict';

(function () {
  'use strict';

  angular.module('ep-frontend').component('surveyResult', {
    templateUrl: '/app/components/survey/survey_result.html',
    controller: SurveyResultCtrl,
    controllerAs: 'sr'
  });
  function SurveyResultCtrl($scope, $state, $stateParams, config, util, dbw, glw) {
    var vm = this;

    vm.project_id = $stateParams.id;
    vm.page = 1;

    vm.branch = '';
    vm.commit = '';
    vm.detail_type = 'bar';

    // set the chart colors here
    var chart_colors = util.combined_chart_colors();
    var charts = {};
    var chart_data = {};
    var hist = {};
    var kv = {};

    // show the question in the detailed-chart view
    var headings = {
      s1: 'How difficult was the exercise for you? (1) Too Easy - (5) Too Difficult',
      s2: 'How interesting was the exercise for you? (1) Very Boring - (5) Very Interesting',
      s3: 'How long was the exercise for you? (1) Too Short - (5) Too Long',
      ratings: 'How would you rate the lab?'
    };

    var detail_pie_options = {
      maintainAspectRatio: false,
      animation: {
        duration: 0
      },
      tooltips: {
        callbacks: {
          // show in fraction of total, you can also use this on the bar chart
          label: function label(tooltipItem, data) {
            var dataset = data.datasets[tooltipItem.datasetIndex];
            var total = dataset.data.reduce(function (previousValue, currentValue, currentIndex, array) {
              return previousValue + currentValue;
            });
            var currentValue = dataset.data[tooltipItem.index];
            var precentage = Math.floor(currentValue / total * 100 + 0.5);
            return precentage + "%";
          }
        }
      }
    };

    var detail_bar_options = {
      maintainAspectRatio: false,
      animation: {
        duration: 0
      },
      legend: {
        display: false
      },
      scales: {
        yAxes: [{
          ticks: {
            // switch this if you want chartjs to normalize the value
            beginAtZero: true,
            min: 0,
            callback: function callback(value, index, values) {
              if (Math.floor(value) === value) return value;
            }
          }
        }]
      }

      //histogram options
    };var histogram_options = {
      maintainAspectRatio: false,
      animation: {
        duration: 0
      },
      legend: {
        display: false
      },
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true,
            min: 0,
            callback: function callback(value, index, values) {
              if (Math.floor(value) === value) return value;
            }
          }
        }]
      }
    };

    function combined_bar_options() {
      return {
        maintainAspectRatio: false,
        animation: {
          duration: 0
        },
        scales: {
          xAxes: [{
            ticks: {
              callback: function callback(value, index, values) {
                if (Math.floor(value) === value) return (value * 100 / vm.feedback.length).toString() + '%';
              },
              min: 0,
              stepSize: Math.floor(vm.feedback.length / 10)
            },
            stacked: true
          }],
          yAxes: [{
            stacked: true
          }]
        },
        tooltips: {
          position: 'nearest',
          callbacks: {
            //show in fraction of total
            label: function label(tooltipItem, data) {
              var dataset = data.datasets[tooltipItem.datasetIndex];
              var total = dataset.data.reduce(function (previousValue, currentValue, currentIndex, array) {
                return previousValue + currentValue;
              });
              var currentValue = dataset.data[tooltipItem.index];
              var precentage = Math.floor(currentValue / total * 100 + 0.5);
              return precentage + "%";
            }
          }
        }
      };
    }

    // select the type of chart for each question, in bar/pie chart
    vm.selectDetail = function () {
      var opt = vm.detail_type == 'pie' ? detail_pie_options : detail_bar_options;
      //this has to use a copy otherwise it will modify the original
      var data = angular.copy(chart_data[vm.show]

      // annotate standard deviation for the bar chart
      //if(vm.detail_type == 'bar')
      //data = annotateStd(vm.show, data)

      );drawChart(vm.detail_type, 'detail_chart', data, opt);
      vm.detail = vm[vm.show];
      vm.detail_heading = headings[vm.show];
    };

    // the bar chart with standard annotation is made of 3 datasets
    // the actual data, data-std, and data+std
    // this is a combined bar chart of the actual data with scatter points
    // of the standard deviations
    function annotateStd(i, chart_data) {
      var tmp = chart_data;
      var std = vm[i].std;
      var val = kv[i].values;

      // value + standard deviation
      var max = {
        type: 'scatter',
        pointBackgroundColor: 'rgba(153, 0, 0, 0.5)',
        showLine: false,
        data: [{ x: 1, y: val[0] + std }, { x: 2, y: val[1] + std }, { x: 3, y: val[2] + std }, { x: 4, y: val[3] + std }, { x: 5, y: val[4] + std }]

        // value - standard deviation
      };var min = {
        type: 'scatter',
        pointBackgroundColor: 'rgba(153, 0, 0, 0.5)',
        showLine: false,
        data: [{ x: 1, y: val[0] - std }, { x: 2, y: val[1] - std }, { x: 3, y: val[2] - std }, { x: 4, y: val[3] - std }, { x: 5, y: val[4] - std }]

        // the dataset we copy this from has multiple colors for combined bar chart
        // now we set the colors of the data set to only one color
      };tmp.datasets.forEach(function (dataset) {
        dataset.backgroundColor = chart_colors.bg[4];
        dataset.borderColor = chart_colors.fg[4];
      });

      tmp.datasets.push(min);
      tmp.datasets.push(max);

      return tmp;
    }

    // draw the chart
    // the last 2 options are optional
    function drawChart(type, id, chart_data, option, title) {

      var ctx = document.getElementById(id).getContext("2d");
      if (typeof charts[id] != 'undefined') charts[id].destroy();

      if (title) option.title = {
        display: true,
        text: title
      };

      charts[id] = new Chart(ctx, {
        type: type,
        data: chart_data,
        options: option
      });
    }

    // process question 1, 2, 3, and ratings
    function process_s(s) {
      var entries = vm.feedback.map(function (item) {
        return item[s];
      });
      vm[s] = {
        entries: entries,
        avg: util.avg(entries),
        median: util.median(entries),
        max: util.max(entries),
        min: util.min(entries),
        std: util.stdeviation(entries)
        // map the histogram values
      };hist[s] = util.histogram_count(vm[s].entries

      // create a key-value pairs
      );kv[s] = util.keys_values(hist[s]

      // create the chart data for chartjs
      );chart_data[s] = {
        datasets: [{
          data: kv[s].values,
          backgroundColor: chart_colors.bg,
          borderColor: chart_colors.fg
        }],
        labels: kv[s].keys
      };
    }

    // filter out empty comments
    function process_comments() {
      vm.comments = vm.feedback.map(function (item) {
        return item.comment;
      }).filter(function (item) {
        return item != null && item != '';
      });
    }

    function process() {
      process_s('s1');
      process_s('s2');
      process_s('s3');
      process_s('s4');
      process_s('s5');
      process_s('rating');
      process_comments

      // histograms for 4 and 5 dont need colors
      ();chart_data['s4'] = {
        datasets: [{
          data: kv['s4'].values
          // if you wanna color the histogram
          //backgroundColor: chart_colors.bg,
          //borderColor: chart_colors.fg
        }],
        labels: kv['s4'].keys
      };

      chart_data['s5'] = {
        datasets: [{
          data: kv['s5'].values
        }],
        labels: kv['s5'].keys
      };

      // stacked horizontal bar chart for question 1, 2, 3 and ratings
      chart_data['combined'] = {
        datasets: [{
          // frequency of answer with value '1'
          label: '1',
          // answer frequency of 'survey1', where the answer is '1', ...
          // if there is no entry it will return null and mess everything
          // do a conditional check ( var || 0) to return zero otherwise
          data: [hist['s1']['1'] || 0, hist['s2']['1'] || 0, hist['s3']['1'] || 0, hist['rating']['1'] || 0],
          backgroundColor: chart_colors.bg[0],
          borderColor: chart_colors.fg[0]
        }, {
          label: '2',
          data: [hist['s1']['2'] || 0, hist['s2']['2'] || 0, hist['s3']['2'] || 0, hist['rating']['2'] || 0],
          backgroundColor: chart_colors.bg[1],
          borderColor: chart_colors.fg[1]
        }, {
          label: '3',
          data: [hist['s1']['3'] || 0, hist['s2']['3'] || 0, hist['s3']['3'] || 0, hist['rating']['3'] || 0],
          backgroundColor: chart_colors.bg[2],
          borderColor: chart_colors.fg[2]
        }, {
          label: '4',
          data: [hist['s1']['4'] || 0, hist['s2']['4'] || 0, hist['s3']['4'] || 0, hist['rating']['4'] || 0],
          backgroundColor: chart_colors.bg[3],
          borderColor: chart_colors.fg[3]
        }, {
          label: '5',
          data: [hist['s1']['5'] || 0, hist['s2']['5'] || 0, hist['s3']['5'] || 0, hist['rating']['5'] || 0],
          backgroundColor: chart_colors.bg[4],
          borderColor: chart_colors.fg[4]
        }],
        labels: [['How difficult was the exercise for you?', '(1) Too Easy - (5) Too Difficult'], ['How interesting was the exercise for you?', '(1) Very Boring - (5) Very Interesting'], ['How long was the exercise for you?', '(1) Too Short - (5) Too Long'], ['How would you rate the lab?']]
      };

      drawChart('bar', 's4_bar', chart_data.s4, histogram_options, 'Time spent on pre-lab');
      drawChart('bar', 's5_bar', chart_data.s5, histogram_options, 'Time spent on lab');
      drawChart('horizontalBar', 'combined_bar', chart_data.combined, combined_bar_options());
    }

    function getLabFeedbacks() {
      var success = function success(res) {
        vm.feedback = res.survey_result.sort(function (a, b) {
          return new Date(b.date).getTime() - new Date(a.date).getTime();
        });
        process();
      };
      dbw.survey_list(vm.project_id, vm.branch, vm.commit, success);
    }

    vm.getCommits = function () {
      var success = function success(res) {
        vm.commits = res;
      };
      glw.commits(vm.project_id, vm.branch, success);
    };

    function getBranches() {
      var success = function success(res) {
        vm.branches = res;
      };
      glw.branches(vm.project_id, success);
    }

    function getProject() {
      var success = function success(res) {
        vm.info = res;
      };
      glw.project(vm.project_id, success);
    }

    vm.refresh = function () {
      vm.feedback.length = 0;
      getLabFeedbacks();
    };

    function init() {
      getLabFeedbacks();
      getProject();
      getBranches();
    }

    init();
  }
})();
'use strict';

(function () {
  'use strict';

  angular.module('ep-frontend').component('surveyToken', {
    templateUrl: '/app/components/survey/survey_token.html',
    controller: SurveyTokenCtrl,
    controllerAs: 'at'
  });
  function SurveyTokenCtrl($scope, $state, $stateParams, config, dbw, glw) {
    var vm = this;

    vm.project_id = $stateParams.id;
    vm.branch = '';
    vm.commit = '';

    vm.new_tokens = function () {
      var success = function success(res) {
        // refresh token list on success
        vm.tokens.length = 0;
        getFeedbackTokens();
      };
      var tokens = [];
      for (var i = 0; i < vm.n; i++) {
        tokens.push({
          token: generateTokens(),
          expiration_date: vm.expire,
          project_id: vm.project_id
        });
      }

      dbw.token_new(tokens, success);
    };

    vm.getCommits = function () {
      var success = function success(res) {
        vm.commits = res;
      };
      glw.commits(vm.project_id, vm.branch, success);
    };

    vm.deleteExpiredToken = function () {
      var success = function success(res) {
        getFeedbackTokens();
      };
      var tokens = vm.expired.join(',');
      dbw.token_delete(tokens, success);
    };

    vm.generateSurveyUrls = function () {
      vm.urls = vm.tokens.map(function (token) {
        return config.hostname + '/#!/survey?' + 'id=' + vm.project_id + '&token=' + token.token + '&branch=' + vm.branch + '&sha=' + vm.commit;
      }).join('\n');
    };

    function getExpiredTokens() {
      var now = new Date();
      vm.expired = vm.tokens.filter(function (token) {
        var token_date = new Date(token.expiration_date);
        return token_date.getTime() < now.getTime();
      }).map(function (token) {
        return token.id;
      });
    }

    function getBranches() {
      var success = function success(res) {
        vm.branches = res;
      };
      glw.branches(vm.project_id, success);
    }

    // you can change this what you need
    function generateTokens() {
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
      for (var i = 0; i < 16; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
      }return text;
    }

    function getFeedbackTokens() {
      var success = function success(res) {
        vm.tokens = res.survey_token;
        getExpiredTokens();
      };
      dbw.token_list(vm.project_id, success);
    }

    function init() {
      getFeedbackTokens();
      getBranches();
    }

    init();
  }
})();
'use strict';

(function () {
  'use strict';

  angular.module('ep-frontend').service('auth', function ($rootScope, $cookies, config) {

    this.isAuthenticated = function () {
      if ($cookies.get('private_token')) return $cookies.get('private_token') != config.portal_token;else return false;
    };

    this.token = function () {
      if ($cookies.get('private_token')) return $cookies.get('private_token');else return config.portal_token;
    };

    this.getUser = function () {
      return $cookies.getObject('user');
    };

    this.setUser = function (obj) {
      $cookies.putObject('user', obj);
      $cookies.put('private_token', obj.private_token);
    };

    this.clear = function () {
      $cookies.remove('user');
      $cookies.remove('private_token');
    };
  });
})();
'use strict';

(function () {
  'use strict';

  angular.module('ep-frontend').service('config', function ($rootScope, $http) {
    //see app.js
    return $rootScope.config;
  });
})();
'use strict';

(function () {
  'use strict';

  angular.module('ep-frontend').service('dbw', function ($log, config, http) {

    var api = config.db_host + '/api.php/';

    // list of survey data
    // project_id is mandatory, while branch and sha is optional
    this.survey_list = function (project_id, branch, sha, scb) {
      var filter = 'filter[]=project_id,eq,' + project_id;
      if (branch) filter += '&filter[]=branch,eq,' + branch;
      if (sha) filter += '&filter[]=sha,eq,' + sha;
      http.get(api + 'survey_result?transform=1&' + filter, {}, scb);
    };

    // submit new survey data entry
    this.survey_new = function (survey, scb) {
      http.post(api + 'survey_result', survey, scb);
    };

    // submit new token(s)
    this.token_new = function (tokens, scb) {
      http.post(api + 'survey_token', tokens, scb);
    };

    // ask if this token exist
    this.token_verify = function (project_id, token, scb) {
      var filter = 'filter[]=project_id,eq,' + project_id + '&filter[]=token,eq,' + token;
      http.get(api + 'survey_token?transform=1&' + filter, {}, scb);
    };

    // list of tokens of this project
    this.token_list = function (project_id, scb) {
      var filter = 'filter[]=project_id,eq,' + project_id;
      http.get(api + 'survey_token?transform=1&' + filter, {}, scb);
    };

    // delete tokens
    this.token_delete = function (tokens, scb) {
      http.delete(api + 'survey_token/' + tokens, scb);
    };
  });
})();
'use strict';

(function () {
  'use strict';

  angular.module('ep-frontend').service('glw', function ($log, config, auth, http) {

    var api = config.git_host + '/' + config.git_api + '/';

    /**
      check services/http-service.js for the wrapper http functions
      all function parameters are required
      every function here has a scb (success callback) as parameter
      which will be called when the request is successful
    */

    // authenticate to GitLab, I use username instead of email
    // https://docs.gitlab.com/ee/api/session.html
    this.login = function (uname, pass, scb) {
      http.post(api + 'session', {
        login: uname,
        password: pass
      }, scb);
    };

    // list repository branches
    // https://docs.gitlab.com/ee/api/branches.html
    this.branches = function (id, scb) {
      http.get_token(auth.token(), api + 'projects/' + id + '/repository/branches', {}, scb);
    };

    // get a single project
    // https://docs.gitlab.com/ee/api/projects.html#get-single-project
    this.project = function (id, scb) {
      http.get_token(auth.token(), api + 'projects/' + id, {}, scb);
    };

    // list projects
    // https://docs.gitlab.com/ee/api/projects.html#list-projects
    this.projects = function (scb) {
      http.get_token(auth.token(), api + 'projects', {}, scb);
    };

    // list of the currently authenticated user's events
    // if you arent logged in this will return the Frontend Portal's feed
    // https://docs.gitlab.com/ee/api/events.html
    this.events = function (scb) {
      http.get_token(auth.token(), api + 'events', {}, scb);
    };

    // list a group's project
    // https://docs.gitlab.com/ee/api/groups.html#list-a-group-s-projects
    this.group_projects = function (id, scb) {
      http.get_token(auth.token(), api + 'groups/' + id + '/projects', {}, scb);
    };

    // list a project's issues
    // https://docs.gitlab.com/ee/api/issues.html#list-project-issues
    this.project_issues = function (id, scb) {
      http.get_token(auth.token(), api + 'projects/' + id + '/issues', {}, scb);
    };

    // list notes (comments) for a single issue
    // https://docs.gitlab.com/ee/api/notes.html#list-project-issue-notes
    this.issue_notes = function (project_id, issue_id, scb) {
      http.get_token(auth.token(), api + 'projects/' + project_id + '/issues/' + issue_id + '/notes', {}, scb);
    };

    // create a new comment in an issue
    // https://docs.gitlab.com/ee/api/notes.html#create-new-issue-note
    this.new_note = function (project_id, issue_id, note, scb) {
      http.post_token(auth.token(), api + 'projects/' + project_id + '/issues/' + issue_id + '/notes', {
        id: project_id,
        issue_id: issue_id,
        body: note
      }, scb);
    };

    // create a commit
    // https://docs.gitlab.com/ee/api/commits.html#create-a-commit-with-multiple-files-and-actions
    this.commit = function (project_id, branch, commit_msg, actions, scb) {
      http.post_token(auth.token(), api + 'projects/' + project_id + '/repository/commits', {
        id: project_id,
        branch: branch,
        commit_message: commit_msg,
        actions: actions
      }, scb);
    };

    // list commits of a project
    // https://docs.gitlab.com/ee/api/commits.html#list-repository-commits
    this.commits = function (project_id, branch, scb) {
      http.get_token(auth.token(), api + 'projects/' + project_id + '/repository/commits', {
        ref_name: branch
      }, scb);
    };

    /* not used */

    // list repository files and directories in a project
    // https://docs.gitlab.com/ee/api/repositories.html#list-repository-tree
    this.tree = function (id, dir, branch, scb) {
      http.get_token(auth.token(), api + 'projects/' + id + '/repository/tree', {
        path: dir,
        ref: branch
      }, scb);
    };

    // get file from repository
    // https://docs.gitlab.com/ee/api/repository_files.html#get-file-from-repository
    this.file = function (id, path, branch, scb) {
      http.get_token(auth.token(), api + 'projects/' + id + '/repository/files/' + path, {
        ref: branch
      }, scb);
    };

    // get raw file from repository
    // https://docs.gitlab.com/ee/api/repository_files.html#get-raw-file-from-repository
    this.download = function (id, path, branch, scb) {
      http.get_token(auth.token(), api + 'projects/' + id + '/repository/files/' + path + '/raw', {
        ref: branch
      }, scb);
    };

    // list groups
    // https://docs.gitlab.com/ee/api/groups.html#list-groups
    this.groups = function (scb) {
      http.get_token(auth.token(), api + 'groups', {}, scb);
    };

    // download the repo as an archive
    // https://docs.gitlab.com/ee/api/repositories.html#get-file-archive
    this.archive = function (project_id, branch, scb) {
      http.get_token(auth.token(), api + 'projects/' + project_id + '/repository/archive', {
        sha: branch
      }, scb);
    };

    // returns the url to download the project as an archive
    this.archive_url = function (project_id, branch) {
      return api + 'projects/' + project_id + '/repository/archive?sha=' + branch + '&private_token=' + auth.token();
    };

    // get raw file contents for a blob by blob SHA
    // https://docs.gitlab.com/ee/api/repositories.html#raw-blob-content
    this.blob = function (project_id, blob_id, branch, scb) {
      http.get_token(auth.token(), api + 'projects/' + project_id + '/repository/blobs/' + blob_id + '/raw', {}, scb);
    };

    // returns the url to raw file contents for a blob
    this.blob_url = function (project_id, blob_id, branch, scb) {
      return api + 'projects/' + project_id + '/repository/blobs/' + blob_id + '/raw';
    };
  });
})();
'use strict';

(function () {
  'use strict';

  angular.module('ep-frontend').service('http', function ($log, $http) {

    // send a post request
    this.post = function (url, data, scb, fcb) {
      var success = function success(res) {
        scb(res.data);
      };
      var fail = function fail(res) {
        if (fcb) fcb(res.data);
      };
      return $http.post(url, data).then(success, fail);
    };

    // send a post request, with the private token attached to headers
    // every GitLab API call use this, except login
    this.post_token = function (token, url, data, scb, fcb) {
      var success = function success(res) {
        scb(res.data);
      };
      var fail = function fail(res) {
        if (fcb) fcb(res.data);
      };
      return $http.post(url, data, {
        headers: {
          "PRIVATE-TOKEN": token
        }
      }).then(success, fail);
    };

    // send a get request
    this.get = function (url, data, scb, fcb) {
      var success = function success(res) {
        scb(res.data);
      };
      var fail = function fail(res) {
        if (fcb) fcb(res.data);
      };
      return $http.get(url, { params: data }).then(success, fail);
    };

    // send a get request, with the private token attached to headers
    // every GET GitLab API call use this
    this.get_token = function (token, url, data, scb, fcb) {
      var success = function success(res) {
        scb(res.data);
      };
      var fail = function fail(res) {
        if (fcb) fcb(res.data);
      };
      return $http.get(url, {
        params: data,
        headers: {
          "PRIVATE-TOKEN": token
        }
      }).then(success, fail);
    };

    // send a put request
    this.put = function (token, url, data, scb, fcb) {
      var success = function success(res) {
        scb(res.data);
      };
      var fail = function fail(res) {
        if (fcb) fcb(res.data);
      };
      return $http.put(url, data, {}).then(success, fail);
    };

    // send a delete request
    // only used to delete token from database
    this.delete = function (url, scb, fcb) {
      var success = function success(res) {
        scb(res.data);
      };
      var fail = function fail(res) {
        if (fcb) fcb(res.data);
      };
      return $http.delete(url, {}).then(success, fail);
    };
  });
})();
'use strict';

(function () {
  'use strict';

  angular.module('ep-frontend').service('util', function ($log) {

    // calculate average of an array of integers
    this.avg = function (arr) {
      var total = arr.reduce(function (a, b) {
        return parseInt(a) + parseInt(b);
      }, 0);
      var n = arr.length;
      return total / n;
    };

    // find median of an array of integers
    this.median = function (arr) {
      var n = arr.length;
      var r = Math.round(n / 2);
      return arr.sort(function (a, b) {
        return b - a;
      })[r];
    };

    // find maximum value of an array of integers
    this.max = function (arr) {
      return arr.reduce(function (a, b) {
        return Math.max(a, b);
      });
    };

    // find minimum value of an array of integers
    this.min = function (arr) {
      return arr.reduce(function (a, b) {
        return Math.min(a, b);
      });
    };

    // calculate standard deviation of an array of integers
    this.stdeviation = function (arr) {
      var avg = this.avg(arr);

      var squareDiffs = arr.map(function (value) {
        var diff = value - avg;
        var sqrDiff = diff * diff;
        return sqrDiff;
      });

      var avgSquareDiff = this.avg(squareDiffs);

      return Math.sqrt(avgSquareDiff);
    };

    // count frequency of appearance
    // e.g. [1,1,1,1, 2, 3,3, 100, 100, 100,100]
    /* returns {
      '1': 4,
      '2': 1,
      '3': 2,
      '100': 4
    } */
    this.histogram_count = function (arr) {
      var tmp = {};
      arr.forEach(function (item) {
        if (tmp[item] != undefined) {
          tmp[item]++;
        } else {
          tmp[item] = 1;
        }
      });
      return tmp;
    };

    // have 5 range of answers (1-5)
    this.blue_chart_colors = function () {
      var colors = ['198, 219, 239', '158, 202, 225', '107, 174, 214', ' 49, 130, 189', '  8,  81, 156'];

      return {
        // foreground, used for border colors
        fg: colors.map(function (color) {
          return 'rgba(' + color + ',1)';
        }),
        // background, used for primary chart body colors
        bg: colors.map(function (color) {
          return 'rgba(' + color + ',0.7)';
        })
      };
    };

    this.combined_chart_colors = function () {
      var colors = ['217,  83,  79', '240, 173,  78', ' 91, 192, 222', ' 40,  96, 144', ' 92, 184,  92'];

      return {
        // foreground, used for border colors
        fg: colors.map(function (color) {
          return 'rgba(' + color + ',1)';
        }),
        // background, used for primary chart body colors
        bg: colors.map(function (color) {
          return 'rgba(' + color + ',0.8)';
        })
      };
    };

    // for pie charts
    this.rating_chart_colors = function () {
      var colors = ['241, 88, 84', // red
      '222, 207, 63', // yellow
      '99, 99, 99', // grey
      '93, 165, 218', // blue
      '96, 189, 104' // green
      ];

      return {
        // foreground, used for border colors
        fg: colors.map(function (color) {
          return 'rgba(' + color + ',1)';
        }),
        // background, used for primary chart body colors
        bg: colors.map(function (color) {
          return 'rgba(' + color + ',0.5)';
        })
      };
    };

    // transform an object with sets of keys and values
    // e.g. { a: 1, b: 2, c:3 }
    /* returns {
      keys: ['a', 'b', 'c'],
      values: ['1', '2', '3']
    } */
    this.keys_values = function (obj) {
      var tmp = {
        keys: [],
        values: []
      };
      Object.keys(obj).forEach(function (key, index) {
        tmp.keys.push(key);
        tmp.values.push(obj[key]);
      });
      return tmp;
    };

    // filter unique values in an array
    // e.g. [1,1,2,3]
    // returns [1,2,3]
    this.onlyUnique = function (arr) {
      return arr.filter(function (value, index, self) {
        return self.indexOf(value) === index;
      });
    };
  });
})();