#!/bin/bash

source config.cfg

pushd $portal_preview_path

response=$(curl -sS -H "Content-Type:application/json" $git_url"/api/v4/groups/"$labs_groupid"/projects?private_token="$token)

project_names=$(echo $response | $jq_path -rc "[.[] | {name:.name}]")

for i in $(echo "${project_names}" | $jq_path -rc '.[]');
do  
  project_name=$(echo $i | $jq_path -rc ${1} '.name')
  if [ -d "$project_name" ]; then
    echo "doing git pull on " $project_name
    pushd $project_name
    git pull
    popd
  else
    echo "doing git clone on " $project_name
    git clone $git_url"/"$labs_groupname"/"$project_name".git"
  fi

done
