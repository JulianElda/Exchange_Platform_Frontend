#!/bin/bash

source config.cfg

path=$1
commit_msg=$2

#Main project

echo "Running git status on "$path
pushd $path

git status
git add --all
git commit -m "$commit_msg"
git push -u

last_commit=$(git log -n 1 $branch --pretty=format:"%H")

popd

echo "Lab project updated"

#Solutions project

read -p "Press any key to update the solutions"

echo "Running git status on "$path"/data/withSolutions"
pushd $path"/data/withSolutions"

echo $last_commit > last_commit

git status
git add --all
git commit -m "$commit_msg"
git push -u

popd

echo "Solutions updated"
