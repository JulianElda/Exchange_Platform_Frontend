#!/bin/bash

source config.cfg

project_name=$1
path=$2

#Main project

echo "Changing working directory to "$path
pushd $path

git clone $git_url"/"$labs_groupname"/"$project_name".git"

popd

echo "Lab project cloned"

#Solutions project

read -p "Press any key to clone the solutions"

pushd $path"/"$project_name"/data"

git clone $git_url"/"$solutions_groupname"/"$project_name"_"$solutions_label".git"

popd

echo "Solutions project cloned"

mv $path"/"$project_name"/data/"$project_name"_"$solutions_label $path"/"$project_name"/data/withSolutions"
