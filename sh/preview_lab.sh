#!/bin/bash

source config.cfg

project_name=$1

pushd $portal_preview_path

if [ -d "$project_name" ]; then
  echo "doing git pull on " $project_name
  pushd $project_name
  git pull
else
  echo "doing git clone on " $project_name
  git clone $git_url"/"$labs_groupname"/"$project_name".git"
fi