#!/bin/bash
# For testing purposes only

git_url=http://ilabxp2.net.in.tum.de:9080
token=woxcQBQJxQFQxLvSRnUf

project_id=$2
path=$1

echo "Deleting project id "$project_id
curl -sS -X "DELETE" $git_url/api/v4/projects/$project_id?private_token=$token

pushd $path

rm -rfv .git/*
rm .gitignore
rmdir .git

popd
