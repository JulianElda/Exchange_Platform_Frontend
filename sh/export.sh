#!/bin/bash

source config.cfg

project_name=$1
path=$2

#Main project

echo "Sending GitLab API to create project"

response=$(curl -sS -H "Content-Type:application/json" $git_url/api/v4/projects?private_token=$token -d \
  "{ \"namespace_id\": \"$labs_groupid\", \"name\": \"$project_name\"  }")

if echo "$response" | grep -q "has already been taken"
then
  echo "Project name already used, try another project name"
  echo "Exitting script"
  exit 0
else
  echo "Creating project "$project_name
fi

project_url=$(echo $response | $jq_path -r ".http_url_to_repo")
project_id=$(echo $response | $jq_path -r ".id")

echo "Running git init on "$path

pushd $path

if [ -d "index.html" ]; then
  echo "Lab preview available"
else
  echo "Creating lab preview file: index.html"
  cp preview.html index.html
fi

if [ -d ".git" ]; then
  echo "Git repository already exist in folder " $path
  echo "Skipping git push"
else
  echo "data/withSolutions" > .gitignore
  git init
  git config user.name  $git_uname
  git config user.email $git_email
  git remote add origin $project_url
  git add .
  git commit -m "initial commit"
  git push -u origin master
fi

popd

echo "Project created on GitLab with id "$project_id

#Feedback project

read -p "Press any key to create the feedback project"

feedback_response=$(curl -sS -H "Content-Type:application/json" $git_url/api/v4/projects?private_token=$token -d "{ \"namespace_id\": \"$feedback_groupid\", \"name\": \"$project_name"_"$feedback_label\"  }")

feedback_project_url=$(echo $feedback_response | $jq_path -r ".http_url_to_repo")
feedback_project_id=$(echo $feedback_response | $jq_path -r ".id")

curl -sS -H "Content-Type:application/json" $git_url/api/v4/projects/$feedback_project_id/repository/commits?private_token=$token -d \
  "{ \"id\": \"$feedback_project_id\", \"branch\": \"master\", \"commit_message\": \"added survey.json\" , \"actions\": [{ \"action\": \"create\", \"file_path\": \"survey.json\", \"content\": \"{}\" }] }"

curl -sS -H "Content-Type:application/json" $git_url/api/v4/projects/$feedback_project_id/issues?private_token=$token -d \
  "{ \"id\": \"$feedback_project_id\", \"title\": \"$project_name"_"$issue_label\", \"labels\": \"$issue_label\" }"

echo "Feedback project created on GitLab with id "$feedback_project_id


#Solutions project

read -p "Press any key to create the solutions"

echo "Creating solutions project for "$project_name

pushd $path"/data/withSolutions"

solutions_response=$(curl -sS -H "Content-Type:application/json" $git_url/api/v4/projects?private_token=$token -d \
  "{ \"namespace_id\": \"$solutions_groupid\", \"name\": \"$project_name"_"$solutions_label\"  }")
solutions_project_url=$(echo $solutions_response | $jq_path -r ".http_url_to_repo")
solutions_project_id=$(echo $solutions_response | $jq_path -r ".id")

if [ -d ".git" ]; then
  echo "Git repository already exist in folder "$path"/data/withSolutions"
  echo "Skipping git push"
else
  git init
  git config user.name  $git_uname
  git config user.email $git_email
  git remote add origin $solutions_project_url
  git add .
  git commit -m "initial commit"
  git push -u origin master
fi

popd

echo "Solutions created on GitLab with id "$solutions_project_id
