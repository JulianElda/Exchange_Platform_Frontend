(function () {
  'use strict';
  angular.module('ep-frontend').component('newsFeed', {
    templateUrl: '/app/components/feed/feed.html',
    controller: FeedCtrl,
    controllerAs: 'nf'
  });
  function FeedCtrl($scope, glw) {
    var vm = this;

    function eventText(event){
      if(event.action_name == 'commented on'){
        return 'New comment on ' + event.target_title
      }
      else if(event.action_name == 'pushed to'){
        return event.data.project.name + ' was updated'
      }
      else if(event.action_name == 'pushed new'){
        return event.data.project.name + ' was created'
      }
    }

    // we only show these events
    // for future works this can be futher implemented & improved
    function getEvents(){
      var success = function(res){
        vm.events = res.filter(function(item){
          return (item.action_name == 'commented on' ||
                  item.action_name == 'pushed to' ||
                  item.action_name == 'pushed new')
        }).map(eventText)
      }
      glw.events(success)
    }

    function init(){
      getEvents()
    }

    init()

  }
})();
