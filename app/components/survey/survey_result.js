(function () {
  'use strict';
  angular.module('ep-frontend').component('surveyResult', {
    templateUrl: '/app/components/survey/survey_result.html',
    controller: SurveyResultCtrl,
    controllerAs: 'sr'
  });
  function SurveyResultCtrl($scope, $state, $stateParams, config, util, dbw, glw) {
    var vm = this;

    vm.project_id = $stateParams.id
    vm.page = 1

    vm.branch = ''
    vm.commit = ''
    vm.detail_type = 'bar'

    // set the chart colors here
    var chart_colors = util.combined_chart_colors()
    var charts = {}
    var chart_data = {}
    var hist = {}
    var kv = {}

    // show the question in the detailed-chart view
    var headings = {
      s1: 'How difficult was the exercise for you? (1) Too Easy - (5) Too Difficult',
      s2: 'How interesting was the exercise for you? (1) Very Boring - (5) Very Interesting',
      s3: 'How long was the exercise for you? (1) Too Short - (5) Too Long',
      ratings: 'How would you rate the lab?'
    }

    var detail_pie_options = {
      maintainAspectRatio: false,
      animation: {
        duration: 0
      },
      tooltips: {
        callbacks: {
          // show in fraction of total, you can also use this on the bar chart
          label: function(tooltipItem, data) {
            var dataset = data.datasets[tooltipItem.datasetIndex];
            var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
              return previousValue + currentValue;
            });
            var currentValue = dataset.data[tooltipItem.index];
            var precentage = Math.floor(((currentValue/total) * 100)+0.5);
            return precentage + "%";
          }
        }
      }
    }

    var detail_bar_options = {
      maintainAspectRatio: false,
      animation: {
        duration: 0
      },
      legend: {
        display: false
      },
      scales: {
        yAxes: [{
          ticks: {
            // switch this if you want chartjs to normalize the value
            beginAtZero: true,
            min: 0,
            callback: function(value, index, values) {
              if (Math.floor(value) === value)
                return value;
            }
          }
        }]
      }
    }

    //histogram options
    var histogram_options = {
      maintainAspectRatio: false,
      animation: {
        duration: 0
      },
      legend: {
        display: false
      },
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true,
            min: 0,
            callback: function(value, index, values) {
              if (Math.floor(value) === value)
                return value;
            }
          }
        }],
      }
    }

    function combined_bar_options() {
      return {
        maintainAspectRatio: false,
        animation: {
          duration: 0
        },
        scales: {
          xAxes: [{
            ticks: {
              callback: function(value, index, values) {
                if (Math.floor(value) === value)
                  return (value * 100/ vm.feedback.length).toString() + '%';
              },
              min: 0,
              stepSize: Math.floor( vm.feedback.length / 10)
            },
            stacked: true
          }],
          yAxes: [{
            stacked: true,
          }]
        },
        tooltips: {
          position: 'nearest',
          callbacks: {
            //show in fraction of total
            label: function(tooltipItem, data) {
              var dataset = data.datasets[tooltipItem.datasetIndex];
              var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                return previousValue + currentValue;
              });
              var currentValue = dataset.data[tooltipItem.index];
              var precentage = Math.floor(((currentValue/total) * 100)+0.5);
              return precentage + "%";
            }
          }
        }
      }
    }

    // select the type of chart for each question, in bar/pie chart
    vm.selectDetail = function(){
      var opt = (vm.detail_type == 'pie') ? detail_pie_options : detail_bar_options
      //this has to use a copy otherwise it will modify the original
      var data = angular.copy(chart_data[vm.show])

      // annotate standard deviation for the bar chart
      //if(vm.detail_type == 'bar')
        //data = annotateStd(vm.show, data)

      drawChart(vm.detail_type, 'detail_chart', data, opt)
      vm.detail = vm[vm.show]
      vm.detail_heading = headings[vm.show]
    }

    // the bar chart with standard annotation is made of 3 datasets
    // the actual data, data-std, and data+std
    // this is a combined bar chart of the actual data with scatter points
    // of the standard deviations
    function annotateStd(i, chart_data){
      var tmp = chart_data
      var std = vm[i].std
      var val = kv[i].values

      // value + standard deviation
      var max = {
        type: 'scatter',
        pointBackgroundColor: 'rgba(153, 0, 0, 0.5)',
        showLine: false,
        data: [ {x:1, y:val[0]+std},
                {x:2, y:val[1]+std},
                {x:3, y:val[2]+std},
                {x:4, y:val[3]+std},
                {x:5, y:val[4]+std} ]
      }

      // value - standard deviation
      var min = {
        type: 'scatter',
        pointBackgroundColor: 'rgba(153, 0, 0, 0.5)',
        showLine: false,
        data: [ {x:1, y:val[0]-std},
                {x:2, y:val[1]-std},
                {x:3, y:val[2]-std},
                {x:4, y:val[3]-std},
                {x:5, y:val[4]-std} ]
      }

      // the dataset we copy this from has multiple colors for combined bar chart
      // now we set the colors of the data set to only one color
      tmp.datasets.forEach(function(dataset){
        dataset.backgroundColor = chart_colors.bg[4]
        dataset.borderColor = chart_colors.fg[4]
      })

      tmp.datasets.push(min)
      tmp.datasets.push(max)

      return tmp
    }

    // draw the chart
    // the last 2 options are optional
    function drawChart(type, id, chart_data, option, title){

      var ctx = document.getElementById(id).getContext("2d");
      if(typeof charts[id] != 'undefined')
        charts[id].destroy()

      if(title)
        option.title = {
          display: true,
          text: title
        }

      charts[id] = new Chart(ctx, {
        type: type,
        data: chart_data,
        options: option
      });
    }

    // process question 1, 2, 3, and ratings
    function process_s(s){
      var entries = vm.feedback.map(function(item){
        return item[s]
      })
      vm[s] = {
        entries: entries,
        avg: util.avg(entries),
        median: util.median(entries),
        max: util.max(entries),
        min: util.min(entries),
        std: util.stdeviation(entries)
      }
      // map the histogram values
      hist[s] = util.histogram_count(vm[s].entries)

      // create a key-value pairs
      kv[s] = util.keys_values(hist[s])

      // create the chart data for chartjs
      chart_data[s] = {
        datasets: [{
          data: kv[s].values,
          backgroundColor: chart_colors.bg,
          borderColor: chart_colors.fg
        }],
        labels: kv[s].keys
      };
    }

    // filter out empty comments
    function process_comments(){
      vm.comments = vm.feedback.map(function(item){
        return item.comment
      }).filter(function(item){
        return item != null && item != ''
      })
    }

    function process(){
      process_s('s1')
      process_s('s2')
      process_s('s3')
      process_s('s4')
      process_s('s5')
      process_s('rating')
      process_comments()

      // histograms for 4 and 5 dont need colors
      chart_data['s4'] = {
        datasets: [{
          data: kv['s4'].values,
          // if you wanna color the histogram
          //backgroundColor: chart_colors.bg,
          //borderColor: chart_colors.fg
        }],
        labels: kv['s4'].keys
      };

      chart_data['s5'] = {
        datasets: [{
          data: kv['s5'].values,
        }],
        labels: kv['s5'].keys
      };

      // stacked horizontal bar chart for question 1, 2, 3 and ratings
      chart_data['combined'] = {
        datasets: [
          {
            // frequency of answer with value '1'
            label: '1',
            // answer frequency of 'survey1', where the answer is '1', ...
            // if there is no entry it will return null and mess everything
            // do a conditional check ( var || 0) to return zero otherwise
            data: [ (hist['s1']['1'] || 0),
                    (hist['s2']['1'] || 0),
                    (hist['s3']['1'] || 0),
                    (hist['rating']['1'] || 0)],
            backgroundColor: chart_colors.bg[0],
            borderColor: chart_colors.fg[0]
          },
          {
            label: '2',
            data: [ (hist['s1']['2'] || 0),
                    (hist['s2']['2'] || 0),
                    (hist['s3']['2'] || 0),
                    (hist['rating']['2'] || 0)],
            backgroundColor: chart_colors.bg[1],
            borderColor: chart_colors.fg[1]
          },
          {
            label: '3',
            data: [ (hist['s1']['3'] || 0),
                    (hist['s2']['3'] || 0),
                    (hist['s3']['3'] || 0),
                    (hist['rating']['3'] || 0)],
            backgroundColor: chart_colors.bg[2],
            borderColor: chart_colors.fg[2]
          },
          {
            label: '4',
            data: [ (hist['s1']['4'] || 0),
                    (hist['s2']['4'] || 0),
                    (hist['s3']['4'] || 0),
                    (hist['rating']['4'] || 0)],
            backgroundColor: chart_colors.bg[3],
            borderColor: chart_colors.fg[3]
          },
          {
            label: '5',
            data: [ (hist['s1']['5'] || 0),
                    (hist['s2']['5'] || 0),
                    (hist['s3']['5'] || 0),
                    (hist['rating']['5'] || 0)],
            backgroundColor: chart_colors.bg[4],
            borderColor: chart_colors.fg[4]
          },
        ],
        labels: [
          ['How difficult was the exercise for you?', '(1) Too Easy - (5) Too Difficult'],
          ['How interesting was the exercise for you?', '(1) Very Boring - (5) Very Interesting'],
          ['How long was the exercise for you?', '(1) Too Short - (5) Too Long'],
          ['How would you rate the lab?'],
        ]
      };

      drawChart('bar', 's4_bar', chart_data.s4, histogram_options, 'Time spent on pre-lab')
      drawChart('bar', 's5_bar', chart_data.s5, histogram_options, 'Time spent on lab')
      drawChart('horizontalBar', 'combined_bar', chart_data.combined, combined_bar_options())
    }

    function getLabFeedbacks(){
      var success = function(res){
        vm.feedback = res.survey_result.sort(function(a,b){
          return (new Date(b.date)).getTime() - (new Date(a.date)).getTime()
        })
        process()
      }
      dbw.survey_list(vm.project_id, vm.branch, vm.commit, success)
    }

    vm.getCommits = function(){
      var success = function(res){
        vm.commits = res
      }
      glw.commits(vm.project_id, vm.branch, success)
    }

    function getBranches(){
      var success = function(res){
        vm.branches = res
      }
      glw.branches(vm.project_id, success)
    }

    function getProject(){
      var success = function(res){
        vm.info = res
      }
      glw.project(vm.project_id, success)
    }

    vm.refresh = function(){
      vm.feedback.length = 0
      getLabFeedbacks()
    }

    function init(){
      getLabFeedbacks()
      getProject()
      getBranches()
    }

    init()

  }
})();
