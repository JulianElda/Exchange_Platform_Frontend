(function () {
  'use strict';
  angular.module('ep-frontend').component('surveyToken', {
    templateUrl: '/app/components/survey/survey_token.html',
    controller: SurveyTokenCtrl,
    controllerAs: 'at'
  });
  function SurveyTokenCtrl($scope, $state, $stateParams, config, dbw, glw) {
    var vm = this;

    vm.project_id = $stateParams.id
    vm.branch = ''
    vm.commit = ''

    vm.new_tokens = function(){
      var success = function(res){
        // refresh token list on success
        vm.tokens.length = 0
        getFeedbackTokens()
      }
      var tokens = []
      for(var i=0; i<vm.n; i++){
        tokens.push({
          token: generateTokens(),
          expiration_date: vm.expire,
          project_id: vm.project_id
        })
      }

      dbw.token_new(tokens, success)
    }

    vm.getCommits = function(){
      var success = function(res){
        vm.commits = res
      }
      glw.commits(vm.project_id, vm.branch, success)
    }

    vm.deleteExpiredToken = function(){
      var success = function(res){
        getFeedbackTokens()
      }
      var tokens = vm.expired.join(',')
      dbw.token_delete(tokens, success)
    }

    vm.generateSurveyUrls = function(){
      vm.urls = vm.tokens.map(function(token){
        return config.hostname + '/#!/survey?'+
          'id='+vm.project_id+
          '&token='+token.token+
          '&branch='+vm.branch+
          '&sha='+vm.commit
      }).join('\n')
    }

    function getExpiredTokens(){
      var now = new Date()
      vm.expired = vm.tokens.filter(function(token){
        var token_date = new Date(token.expiration_date)
        return token_date.getTime() < now.getTime()
      }).map(function(token){
        return token.id
      })
    }

    function getBranches(){
      var success = function(res){
        vm.branches = res
      }
      glw.branches(vm.project_id, success)
    }

    // you can change this what you need
    function generateTokens(){
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
      for (var i = 0; i < 16; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

      return text;
    }

    function getFeedbackTokens(){
      var success = function(res){
        vm.tokens = res.survey_token
        getExpiredTokens()
      }
      dbw.token_list(vm.project_id, success)
    }

    function init(){
      getFeedbackTokens()
      getBranches()
    }

    init()

  }
})();
