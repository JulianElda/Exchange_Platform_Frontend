(function () {
  'use strict';
  angular.module('ep-frontend').component('surveyExport', {
    templateUrl: '/app/components/survey/survey_export.html',
    controller: SurveyExportCtrl,
    controllerAs: 'se'
  });
  function SurveyExportCtrl($scope, $state, $stateParams, FileSaver, config, dbw, glw) {
    var vm = this;

    vm.project_id = $stateParams.id
    vm.feedback_project_id = $stateParams.feedback
    vm.branch = ''
    vm.commit = ''

    // make a commit to _feedback project to save the survey data
    // now it only commits to master branch
    // also, this will fail if the file doesnt exist
    // this file is commited in the initial script, but you might want to make sure it exist
    vm.export_git = function(){
      var success = function(res){
        vm.git_success = true
      }

      glw.commit(vm.feedback_project_id, 'master', vm.commit_msg, [
        {
          action: 'update',
          file_path: config.survey_backup_name,
          content: JSON.stringify(vm.results)
        }
      ] , success)

    }

    // save to browser
    vm.export_json = function(){
      var text = JSON.stringify(vm.results)
      var data = new Blob([text], { type: 'application/json;charset=utf-8' });
      FileSaver.saveAs(data, vm.filename+'.json');
    }

    // remember that the frontend filters by SHORT ID not the long id
    vm.getCommits = function(){
      var success = function(res){
        vm.commits = res
      }
      glw.commits(vm.project_id, vm.branch, success)
    }

    function getBranches(){
      var success = function(res){
        vm.branches = res
      }
      glw.branches(vm.project_id, success)
    }

    vm.getLabFeedbacks = function(){
      var success = function(res){
        vm.results = res.survey_result
      }
      dbw.survey_list(vm.project_id, vm.branch, vm.commit, success)
    }

    // not used, in case you wanna export the token too
    function getFeedbackTokens(){
      var success = function(res){
        vm.tokens = res.survey_token
      }
      dbw.token_list(vm.project_id, success)
    }

    function init(){
      getBranches()
    }

    init()

  }
})();
