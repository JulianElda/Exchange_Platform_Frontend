(function () {
  'use strict';
  angular.module('ep-frontend').component('courseSurvey', {
    templateUrl: '/app/components/survey/survey_form.html',
    controller: CourseSurveyCtrl,
    controllerAs: 'cs'
  });
  function CourseSurveyCtrl($scope, $state, $stateParams, config, dbw, glw) {
    var vm = this;

    vm.project_id = $stateParams.id
    vm.token = $stateParams.token

    // save the survey answers to database, then delete the token
    vm.submitSurvey = function(){
      var success = function(res){
        updateToken()
      }
      vm.survey.branch = $stateParams.branch
      vm.survey.sha = $stateParams.sha
      vm.survey.project_id = $stateParams.id
      vm.survey.s1 = parseInt(vm.survey.s1)
      vm.survey.s2 = parseInt(vm.survey.s2)
      vm.survey.s3 = parseInt(vm.survey.s3)

      dbw.survey_new(vm.survey, success)
    }

    // delete used token, then go back to the lab page
    function updateToken(){
      var success = function(res){
        $state.go('lab', {id:vm.project_id})
      }

      dbw.token_delete(vm.token, success)
    }

    function getProject(){
      var success = function(res){
        vm.info = res
      }
      glw.project(vm.project_id, success)
    }

    function init(){
      getProject()
    }

    init()

  }
})();
