(function () {
  'use strict';
  angular.module('ep-frontend').component('surveyConfirm', {
    templateUrl: '/app/components/survey/survey_confirm.html',
    controller: SurveyConfirmCtrl,
    controllerAs: 'sc'
  });
  function SurveyConfirmCtrl($scope, $state, $stateParams, config, dbw, glw) {
    var vm = this;

    vm.project_id = $stateParams.id
    vm.branch = $stateParams.branch
    vm.sha = $stateParams.sha
    vm.token = $stateParams.token

    vm.confirm = function(){
      $state.go('survey_form', {
        id:vm.project_id,
        token: vm.token_id,
        branch: vm.branch,
        sha: vm.sha
      })
    }

    function getProject(){
      var success = function(res){
        vm.info = res
      }
      glw.project(vm.project_id, success)
    }

    // send a query to the db asking for this token
    // if it returns something then the token is valid
    function verifyToken(){
      var success = function(res){
        var valid = res.survey_token
        if(valid.length < 1)
          vm.invalid = true

        var token_date = new Date(valid[0].expiration_date)
        var now = new Date()

        if( now.getTime() > token_date.getTime() )
          vm.invalid = true

        vm.token_id = valid[0].id
      }
      dbw.token_verify(vm.project_id, vm.token, success)
    }

    function init(){
      getProject()
      verifyToken()
    }

    init()

  }
})();
