(function () {
  'use strict';
  angular.module('ep-frontend').component('mainHeader', {
    templateUrl: '/app/components/nav/header.html',
    controller: HeaderCtrl,
    controllerAs: 'hc'
  });
  function HeaderCtrl($rootScope, $state, glw, auth) {
    var vm = this

    vm.login = function(){
      var success = function(res){
        vm.form = false
        vm.username = ''
        vm.password = ''

        //set private token to cookie
        auth.setUser(res)
        vm.user = res

        $state.go('catalog')
      }
      glw.login(vm.username, vm.password, success)
    }

    vm.logout = function(){
      auth.clear()
      vm.user = undefined

      $state.go('catalog')
    }

    vm.user = auth.getUser()

  }
})();
