(function () {
  'use strict';
  angular.module('ep-frontend').component('labInfo', {
    templateUrl: '/app/components/lab/lab.html',
    controller: LabInfoCtrl,
    controllerAs: 'li'
  });
  function LabInfoCtrl($scope, $state, $stateParams, config, glw) {
    var vm = this;

    vm.project_id = $stateParams.id

    vm.prefix = config.lab_preview_path_prefix + '/'

    vm.permissions = {}
    vm.loading = {}

    vm.download = function(item){
      var success = function(res){
      }
      glw.blob(item.id, 'master', success)
    }

    // not used at the moment
    // returns the download link of the project in archive format
    vm.downloadRepo = function(branch){
      return glw.archive_url(vm.project_id, branch.name)
    }

    function getBranches(){
      var success = function(res){
        vm.branches = res
      }
      glw.branches(vm.project_id, success)
    }

    function getIssues(){
      var success = function(res){
        //there might be other issues created in GitLab, so we filter by name
        vm.issue_id = res.filter(function(item){
          return item.title == vm.lab.name + config.issue_comments_name
        })[0].iid

        vm.ids = {
          project_id: vm.project_id,
          issue_id: vm.issue_id,
          feedback_project_id: vm.feedback_project_id
        }

        vm.loading.issue = true
      }
      glw.project_issues(vm.feedback_project_id, success)
    }

    // these are the unfortunate drawback of the schema, since
    // when I get the lab info, I have to find the solutions/feedback
    // id by filtering them by name like this

    function getFeedback(){
      var success = function(res){
        vm.feedback_project_id = res.filter(function(item){
          return item.name == vm.lab.name + config.feedback_group_name
        })[0].id
        getIssues()
      }
      glw.group_projects(config.feedback_group_id, success)
    }

    function getSolutions(){
      var success = function(res){
        vm.permissions.solutions = true
        vm.solutions_project_id = res.filter(function(item){
          return item.name == vm.lab.name + config.solutions_group_name
        })[0].id
      }
      glw.group_projects(config.solutions_group_id, success)
    }

    // get the lab information and its solutions and feedback project
    function getProject(){
      var success = function(res){
        vm.lab = res

        // if you have at least Master permission
        // see https://docs.gitlab.com/ee/api/members.html
        if( (vm.lab.permissions.group_access != null &&
              vm.lab.permissions.group_access.access_level >= 40)  ||
            (vm.lab.permissions.project_access != null &&
              vm.lab.permissions.project_access.access_level >= 40) )
          vm.permissions.survey = true

        // we only want to get the id of these projects
        getSolutions()
        getFeedback()
      }
      glw.project(vm.project_id, success)
    }

    function init(){
      getProject()
    }

    init()

  }
})();
