(function () {
  'use strict';
  angular.module('ep-frontend').component('labComments', {
    bindings: {
      ids: '<'
    },
    templateUrl: '/app/components/lab/comments.html',
    controller: LabCommentsCtrl,
    controllerAs: 'lc'
  });
  function LabCommentsCtrl($scope, auth, config, glw) {
    var vm = this;

    vm.is_auth = auth.isAuthenticated()

    vm.postComments = function(){
      var success = function(res){
        vm.comments.length = 0
        getComments()
      }
      glw.new_note(vm.feedback_project_id, vm.issue_id, vm.new_comment, success)
    }

    vm.humanizeComment = function(dt){
      return moment(dt).fromNow()
    }

    function getComments(){
      var success = function(res){
        vm.comments = res
      }
      glw.issue_notes(vm.feedback_project_id, vm.issue_id, success)
    }

    // unlike every other module, this has to run on vm..$onInit()
    // since it loads after the /lab page finishes loading
    vm.$onInit = function(){
      vm.feedback_project_id = vm.ids.feedback_project_id
      vm.issue_id = vm.ids.issue_id
      getComments()
    }

  }
})();
