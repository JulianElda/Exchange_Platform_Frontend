(function () {
  'use strict';
  angular.module('ep-frontend').component('labSolutions', {
    templateUrl: '/app/components/lab/solutions.html',
    controller: LabSolutionsCtrl,
    controllerAs: 'ls'
  });
  function LabSolutionsCtrl($scope, $state, $stateParams, config, glw) {
    var vm = this;

    vm.project_id = $stateParams.id
    vm.solutions_project_id = $stateParams.solution

    vm.permissions = {}
    vm.loading = {}

    function getProject(){
      var success = function(res){
        vm.solution = res
      }
      glw.project(vm.solutions_project_id, success)
    }

    function init(){
      getProject()
    }

    init()

  }
})();
