(function () {
  'use strict';
  angular.module('ep-frontend').component('catalog', {
    templateUrl: '/app/components/catalog/catalog.html',
    controller: CatalogCtrl,
    controllerAs: 'cc'
  });
  function CatalogCtrl($scope, $state, util, config, glw) {
    var vm = this;

    vm.selectedTag = ''
    vm.labs = []

    vm.tagFilter = function(item){
      if(vm.selectedTag != '')
        return item.tag_list.includes(vm.selectedTag)
      else
        return item
    }

    // get labs, i.e. projects
    // since the labs in the catalog are in _labs group
    // we get the list of projects in that _labs group
    function getLabs(){
      var success = function(res){
        vm.labs = vm.labs.concat(res)
        //map all the tag_list of a project for filtering
        var tag_list = vm.labs.map(function(project){
          return project.tag_list
        })
        vm.tags = [].concat.apply([], tag_list)
        vm.tags = util.onlyUnique(vm.tags)
      }
      glw.group_projects(config.labs_group_id, success)
    }

    function init(){
      getLabs()
    }

    init()

  }
})();
