(function(){
  'use strict';
  angular.module('ep-frontend').service('dbw', function($log, config, http) {

    var api = config.db_host+'/api.php/'

    // list of survey data
    // project_id is mandatory, while branch and sha is optional
    this.survey_list = function(project_id, branch, sha, scb){
      var filter = 'filter[]=project_id,eq,'+project_id
      if(branch)
        filter += '&filter[]=branch,eq,'+branch
      if(sha)
        filter += '&filter[]=sha,eq,'+sha
      http.get(api +'survey_result?transform=1&'
        +filter, {}, scb)
    }

    // submit new survey data entry
    this.survey_new = function(survey, scb){
      http.post(api +'survey_result', survey, scb)
    }

    // submit new token(s)
    this.token_new = function(tokens, scb){
      http.post(api +'survey_token', tokens, scb)
    }

    // ask if this token exist
    this.token_verify = function(project_id, token, scb){
      var filter = 'filter[]=project_id,eq,'+project_id +
        '&filter[]=token,eq,'+token
      http.get(api +'survey_token?transform=1&'
        +filter, {}, scb)
    }

    // list of tokens of this project
    this.token_list = function(project_id, scb){
      var filter = 'filter[]=project_id,eq,'+project_id
      http.get(api +'survey_token?transform=1&'
        +filter, {}, scb)
    }

    // delete tokens
    this.token_delete = function(tokens, scb){
      http.delete(api +'survey_token/'+tokens, scb)
    }

  });
})();
