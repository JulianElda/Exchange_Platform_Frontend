(function(){
  'use strict';
  angular.module('ep-frontend').service('glw', function($log, config, auth, http) {

    var api = config.git_host+'/'+config.git_api+'/'

    /**
      check services/http-service.js for the wrapper http functions
      all function parameters are required
      every function here has a scb (success callback) as parameter
      which will be called when the request is successful
    */

    // authenticate to GitLab, I use username instead of email
    // https://docs.gitlab.com/ee/api/session.html
    this.login = function(uname, pass, scb){
      http.post(api+'session', {
        login: uname,
        password:pass
      }, scb)
    }

    // list repository branches
    // https://docs.gitlab.com/ee/api/branches.html
    this.branches = function(id, scb){
      http.get_token(auth.token(), api+'projects/'+id+'/repository/branches', {}, scb)
    }

    // get a single project
    // https://docs.gitlab.com/ee/api/projects.html#get-single-project
    this.project = function(id, scb){
      http.get_token(auth.token(), api+'projects/'+id, {}, scb)
    }

    // list projects
    // https://docs.gitlab.com/ee/api/projects.html#list-projects
    this.projects = function(scb){
      http.get_token(auth.token(), api+'projects', {}, scb)
    }

    // list of the currently authenticated user's events
    // if you arent logged in this will return the Frontend Portal's feed
    // https://docs.gitlab.com/ee/api/events.html
    this.events = function(scb){
      http.get_token(auth.token(), api+'events', {}, scb)
    }

    // list a group's project
    // https://docs.gitlab.com/ee/api/groups.html#list-a-group-s-projects
    this.group_projects = function(id, scb){
      http.get_token(auth.token(), api+'groups/'+id+'/projects', {}, scb)
    }

    // list a project's issues
    // https://docs.gitlab.com/ee/api/issues.html#list-project-issues
    this.project_issues = function(id, scb){
      http.get_token(auth.token(), api+'projects/'+id+'/issues', {}, scb)
    }

    // list notes (comments) for a single issue
    // https://docs.gitlab.com/ee/api/notes.html#list-project-issue-notes
    this.issue_notes = function(project_id, issue_id, scb){
      http.get_token(auth.token(), api+'projects/'+project_id+'/issues/'+issue_id+'/notes', {}, scb)
    }

    // create a new comment in an issue
    // https://docs.gitlab.com/ee/api/notes.html#create-new-issue-note
    this.new_note = function(project_id, issue_id, note, scb){
      http.post_token(auth.token(), api+'projects/'+project_id+'/issues/'+issue_id+'/notes', {
        id: project_id,
        issue_id: issue_id,
        body: note
      }, scb)
    }

    // create a commit
    // https://docs.gitlab.com/ee/api/commits.html#create-a-commit-with-multiple-files-and-actions
    this.commit = function(project_id, branch, commit_msg, actions, scb){
      http.post_token(auth.token(), api+'projects/'+project_id+'/repository/commits', {
        id: project_id,
        branch: branch,
        commit_message: commit_msg,
        actions: actions
      }, scb)
    }

    // list commits of a project
    // https://docs.gitlab.com/ee/api/commits.html#list-repository-commits
    this.commits = function(project_id, branch, scb){
      http.get_token(auth.token(), api+'projects/'+project_id+'/repository/commits', {
        ref_name: branch
      }, scb)
    }




    /* not used */



    // list repository files and directories in a project
    // https://docs.gitlab.com/ee/api/repositories.html#list-repository-tree
    this.tree = function(id, dir, branch, scb){
      http.get_token(auth.token(), api+'projects/'+id+'/repository/tree', {
        path: dir,
        ref: branch
      }, scb)
    }

    // get file from repository
    // https://docs.gitlab.com/ee/api/repository_files.html#get-file-from-repository
    this.file = function(id, path, branch, scb){
      http.get_token(auth.token(), api+'projects/'+id+'/repository/files/'+path, {
        ref: branch
      }, scb)
    }

    // get raw file from repository
    // https://docs.gitlab.com/ee/api/repository_files.html#get-raw-file-from-repository
    this.download = function(id, path, branch, scb){
      http.get_token(auth.token(), api+'projects/'+id+'/repository/files/'+path+'/raw', {
        ref: branch
      }, scb)
    }

    // list groups
    // https://docs.gitlab.com/ee/api/groups.html#list-groups
    this.groups = function(scb){
      http.get_token(auth.token(), api+'groups', {}, scb)
    }

    // download the repo as an archive
    // https://docs.gitlab.com/ee/api/repositories.html#get-file-archive
    this.archive = function(project_id, branch, scb){
      http.get_token(auth.token(), api+'projects/'+project_id+'/repository/archive', {
        sha: branch
      }, scb)
    }

    // returns the url to download the project as an archive
    this.archive_url = function(project_id, branch){
      return api+'projects/'+project_id+'/repository/archive?sha='+branch+
        '&private_token='+auth.token()
    }

    // get raw file contents for a blob by blob SHA
    // https://docs.gitlab.com/ee/api/repositories.html#raw-blob-content
    this.blob = function(project_id, blob_id, branch, scb){
      http.get_token(auth.token(), api+'projects/'+project_id+'/repository/blobs/'+blob_id+'/raw', {}, scb)
    }

    // returns the url to raw file contents for a blob
    this.blob_url = function(project_id, blob_id, branch, scb){
      return api+'projects/'+project_id+'/repository/blobs/'+blob_id+'/raw'
    }
  });
})();
