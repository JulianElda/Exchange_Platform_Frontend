(function(){
  'use strict';
  angular.module('ep-frontend')
  .service('auth', function($rootScope, $cookies, config) {

    this.isAuthenticated = function(){
      if($cookies.get('private_token'))
        return $cookies.get('private_token') != config.portal_token
      else
        return false
    }

    this.token = function(){
      if($cookies.get('private_token'))
        return $cookies.get('private_token')
      else
        return config.portal_token
    }

    this.getUser = function(){
      return $cookies.getObject('user')
    }

    this.setUser = function(obj){
      $cookies.putObject('user', obj)
      $cookies.put('private_token', obj.private_token)
    }

    this.clear = function(){
      $cookies.remove('user')
      $cookies.remove('private_token')
    }

  });
})();
