(function(){
  'use strict';
  angular.module('ep-frontend').service('util', function($log) {

    // calculate average of an array of integers
    this.avg = function(arr){
      var total = arr.reduce(function(a,b){
        return parseInt(a) + parseInt(b)
      },0)
      var n = arr.length
      return total / n
    }

    // find median of an array of integers
    this.median = function(arr){
      var n = arr.length
      var r = Math.round(n/2)
      return arr.sort(function(a,b){
        return b-a
      })[r]
    }

    // find maximum value of an array of integers
    this.max = function(arr){
      return arr.reduce(function(a,b) {
        return Math.max(a, b)
      })
    }

    // find minimum value of an array of integers
    this.min = function(arr){
      return arr.reduce(function(a,b) {
        return Math.min(a, b)
      })
    }

    // calculate standard deviation of an array of integers
    this.stdeviation = function(arr){
      var avg = this.avg(arr);

      var squareDiffs = arr.map(function(value){
        var diff = value - avg;
        var sqrDiff = diff * diff;
        return sqrDiff;
      });

      var avgSquareDiff = this.avg(squareDiffs);

      return Math.sqrt(avgSquareDiff);
    }

    // count frequency of appearance
    // e.g. [1,1,1,1, 2, 3,3, 100, 100, 100,100]
    /* returns {
      '1': 4,
      '2': 1,
      '3': 2,
      '100': 4
    } */
    this.histogram_count = function(arr){
      var tmp = {}
      arr.forEach(function(item){
        if (tmp[item] != undefined){
          tmp[item]++
        }
        else {
          tmp[item] = 1
        }
      })
      return tmp;
    }

    // have 5 range of answers (1-5)
    this.blue_chart_colors = function(){
      var colors = [
        '198, 219, 239',
        '158, 202, 225',
        '107, 174, 214',
        ' 49, 130, 189',
        '  8,  81, 156',
      ];

      return {
        // foreground, used for border colors
        fg: colors.map(function(color){
          return 'rgba(' + color + ',1)'
        }),
        // background, used for primary chart body colors
        bg: colors.map(function(color){
          return 'rgba(' + color + ',0.7)'
        })
      };
    }

    this.combined_chart_colors = function(){
      var colors = [
        '217,  83,  79',
        '240, 173,  78',
        ' 91, 192, 222',
        ' 40,  96, 144',
        ' 92, 184,  92',
      ];

      return {
        // foreground, used for border colors
        fg: colors.map(function(color){
          return 'rgba(' + color + ',1)'
        }),
        // background, used for primary chart body colors
        bg: colors.map(function(color){
          return 'rgba(' + color + ',0.8)'
        })
      };
    }

    // for pie charts
    this.rating_chart_colors = function(){
      var colors = [
        '241, 88, 84', // red
        '222, 207, 63', // yellow
        '99, 99, 99', // grey
        '93, 165, 218', // blue
        '96, 189, 104' // green
      ];

      return {
        // foreground, used for border colors
        fg: colors.map(function(color){
          return 'rgba(' + color + ',1)'
        }),
        // background, used for primary chart body colors
        bg: colors.map(function(color){
          return 'rgba(' + color + ',0.5)'
        })
      };
    }

    // transform an object with sets of keys and values
    // e.g. { a: 1, b: 2, c:3 }
    /* returns {
      keys: ['a', 'b', 'c'],
      values: ['1', '2', '3']
    } */
    this.keys_values = function(obj){
      var tmp = {
        keys: [],
        values: []
      }
      Object.keys(obj).forEach(function(key,index) {
        tmp.keys.push(key);
        tmp.values.push(obj[key])
      });
      return tmp;
    }

    // filter unique values in an array
    // e.g. [1,1,2,3]
    // returns [1,2,3]
    this.onlyUnique = function(arr){
      return arr.filter(function(value, index, self) {
        return self.indexOf(value) === index
      });
    }

  });
})();
