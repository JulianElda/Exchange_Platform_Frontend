(function(){
  'use strict';
  angular.module('ep-frontend').service('http', function($log, $http) {

    // send a post request
    this.post = function(url, data, scb, fcb){
      var success = function(res){
        scb(res.data)
      }
      var fail = function(res){
        if(fcb)
          fcb(res.data)
      }
      return $http.post(url, data).then(success, fail);
    }

    // send a post request, with the private token attached to headers
    // every GitLab API call use this, except login
    this.post_token = function(token, url, data, scb, fcb){
      var success = function(res){
        scb(res.data)
      }
      var fail = function(res){
        if(fcb)
          fcb(res.data)
      }
      return $http.post(url, data, {
        headers:{
          "PRIVATE-TOKEN": token
        }
      }).then(success, fail);
    }

    // send a get request
    this.get = function(url, data, scb, fcb){
      var success = function(res){
        scb(res.data)
      }
      var fail = function(res){
        if(fcb)
          fcb(res.data)
      }
      return $http.get(url, {params: data}).then(success, fail);
    }

    // send a get request, with the private token attached to headers
    // every GET GitLab API call use this
    this.get_token = function(token, url, data, scb, fcb){
      var success = function(res){
        scb(res.data)
      }
      var fail = function(res){
        if(fcb)
          fcb(res.data)
      }
      return $http.get(url, {
        params: data,
        headers: {
          "PRIVATE-TOKEN": token
        }
      }).then(success, fail);
    }

    // send a put request
    this.put = function(token, url, data, scb, fcb){
      var success = function(res){
        scb(res.data)
      }
      var fail = function(res){
        if(fcb)
          fcb(res.data)
      }
      return $http.put(url, data, {}).then(success, fail);
    }

    // send a delete request
    // only used to delete token from database
    this.delete = function(url, scb, fcb){
      var success = function(res){
        scb(res.data)
      }
      var fail = function(res){
        if(fcb)
          fcb(res.data)
      }
      return $http.delete(url, {}).then(success, fail);
    }

  });
})();
