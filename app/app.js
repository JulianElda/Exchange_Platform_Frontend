(function () {
  var app = angular.module('ep-frontend', [
    'ui.router',
    'ui.bootstrap',
    'ngCookies',
    'ngFileSaver'
  ]);
  app.config(function ($urlRouterProvider, $stateProvider, $locationProvider) {

    // define the states and their parameters
    // https://github.com/angular-ui/ui-router/wiki/URL-Routing
    $stateProvider.state({
      name: 'catalog',
      url: '/',
      template: '<catalog></catalog>'
    }).state({
      name: 'lab',
      url: '/lab/:id',
      params: {
        id: null
      },
      template: '<lab-info></lab-info>'
    }).state({
      name: 'solutions',
      params: {
        id: null,
        solution: null
      },
      template: '<lab-solutions></lab-solutions>'
    }).state({
      name: 'survey_confirm',
      url: '/survey?id&token&branch&sha',
      template: '<survey-confirm></survey-confirm>'
    }).state({
      name: 'survey_export',
      params: {
        id: null,
        feedback: null,
      },
      template: '<survey-export></survey-export>'
    }).state({
      name: 'survey_form',
      params: {
        id: null,
        token: null,
        branch: null,
        sha: null,
      },
      template: '<course-survey></course-survey>'
    }).state({
      name: 'survey_result',
      params: {
        id: null,
      },
      template: '<survey-result></survey-result>'
    }).state({
      name: 'survey_token',
      params: {
        id: null,
      },
      template: '<survey-token></survey-token>'
    }).state({
      name: 'help',
      url: '/help',
      template: '<help-page></help-page>'
    });

    $urlRouterProvider.otherwise('/');

    // TODO enable html5 mode to remove hashbang in the url
    // https://stackoverflow.com/questions/22102815/how-to-delete-sign-in-angular-ui-router-urls
    //$locationProvider.html5Mode(true);
  })
  .run(function($rootScope, $http){
    // get frontend configuration
    /*
    $http.get('/config.json').then(function(data){
      $rootScope.config = data.data;
    })
    */

    //temporary fix
    $rootScope.config = {
      "hostname": "http://ilabxp2.net.in.tum.de",
      "git_host": "http://ilabxp2.net.in.tum.de:9080",
      "git_api": "api/v4",
      "db_host": "http://ilabxp2.net.in.tum.de",
      "portal_token": "3kQUqRuDGfmrr51JqYj8",
      "portal_uid": "1331504",
      "portal_comment_alias": "Anonymous",
      "lab_preview_path_prefix": "preview",
      "issue_comments_name": "_comments",
      "labs_group_id": 3,
      "solutions_group_id": 4,
      "solutions_group_name": "_solutions",
      "feedback_group_id": 6,
      "feedback_group_name": "_feedback",
      "survey_backup_name": "survey.json"
    }

  });


})();
