var lazypipe = require('lazypipe');
var babel = require('gulp-babel');
module.exports = {
  bundle: {
    'js/app.bundle': {
      scripts: [
        'app/*.js',
        'app/components/*.js',
        'app/components/**/*.js',
        'app/services/*.js',
        'app/directives/*.js'
      ],
      options: {
        uglify: false,
        rev: false,
        maps: false,
        transforms: {
          scripts: lazypipe().pipe(babel, {
            presets: ['es2015']
          })
        }
      }
    },
    'css/app.bundle': {
      styles: [
        'assets/lib/bootstrap/dist/css/bootstrap.min.css',
        'assets/lib/font-awesome/css/font-awesome.min.css',
        'assets/css/style.css'
        ],
      options: {
        rev: false,
        maps: false
      }
    },
    'js/vendor.bundle': {
      scripts: [
        'assets/lib/angular/angular.min.js',
        'assets/lib/jquery/dist/jquery.min.js',
        'assets/lib/bootstrap/dist/js/bootstrap.min.js',
        'assets/lib/chart.js/dist/Chart.min.js',
        'assets/lib/file-saver/FileSaver.min.js',
        'assets/lib/jquery-slimscroll/jquery.slimscroll.min.js',
        'assets/lib/angular-bootstrap/ui-bootstrap-tpls.min.js',
        'assets/lib/angular-cookies/angular-cookies.min.js',
        'assets/lib/angular-ui-router/release/angular-ui-router.min.js',
        'assets/lib/angular-file-saver/dist/angular-file-saver.min.js',
        'assets/lib/moment/min/moment.min.js',
      ],
      options: {
        uglify: false, // uglify the resulting bundle in prod
        rev: false, // rev the resulting bundle in prod,
        maps: false
      }
    },
  },
  copy: [{
    src: 'assets/lib/font-awesome/fonts/fontawesome-webfont.*',
    base: 'assets/lib/font-awesome/'
  }, {
    src: 'assets/lib/bootstrap/dist/fonts/glyphicons-halflings-regular.*',
    base: 'assets/lib/bootstrap/dist/'
  }]
};
