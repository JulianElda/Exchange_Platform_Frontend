var gulp = require('gulp'),
  webserver = require('gulp-webserver'),
  markdown = require('gulp-markdown'),
  bundle = require('gulp-bundle-assets'),
  rimraf = require('gulp-rimraf'),
  templateCache = require('gulp-angular-templatecache');

var dir = {
  bundle: {
    src: './app/',
    index: './index.html',
    js: ['app/*.js', 'app/**/*.js', 'app/**/**/*.js'],
    html: './app/**/**/*.html',
    dest: './assets/bundle',
  }
};

gulp.task('help-md', function () {
  return gulp.src('help.md')
    .pipe(markdown())
    .pipe(gulp.dest('app/components/help'));
});

gulp.task('webserver', function () {
  gulp.src('./').pipe(webserver());
  gulp.start('watch');
});

gulp.task('clean', function () {
  return gulp.src(dir.bundle.dest, {
    read: false
  })
  .pipe(rimraf());
});

gulp.task('bundle', ['clean'], function () {
  return gulp.src('./gulp/bundle.config.js')
    .pipe(bundle())
    .pipe(gulp.dest(dir.bundle.dest));
});

gulp.task('watch', function () {
  gulp.watch(dir.bundle.js.concat(['./gulp/bundle.config.js', dir.bundle.html]), ['bundle']);
});
