### Can I post comments on the labs?

Yes, first you need to login using your [GitLab](http://ilabxp2.net.in.tum.de:9080/) credentials. Use `username` instead of email address.


### How do I checkout a lab?

Go to the lab page from the catalog and scroll down to *Access*.

Clone the repository via HTTPS or SSH
```
// https
git clone http://ilabxp2.net.in.tum.de:9080/ilab_labs/IN001_iLab1.git

// ssh
git clone git@ilabxp2.net.in.tum.de:ilab_labs/IN001_iLab1.git
```

Access to [GitLab](http://ilabxp2.net.in.tum.de:9080/) is required to checkout the projects.


### How do I checkout the solutions?

The solutions to each lab is stored in a separate project. Access to the solutions in GitLab is required. Login with your GitLab credentials from top right navigation.

For the solutions, go to the directory where the lab is cloned from Git. Create a new directory `withSolutions` inside `data` directory. Navigate to this directory and clone the solutions project.
